import java.io.File

import scala.util.Random
import scala.xml._
import scala.xml.transform.{RewriteRule, RuleTransformer}

object TestCenterXmlAnswerRandomizer {

  /**
   * creates a recursive stream of files
   * @param dir
   * @return
   */
  def fileStream(dir: File): LazyList[File] =
    Option(dir.listFiles).map(_.toList.sortBy(_.getName).to(LazyList))
      .map(files => files lazyAppendedAll files.filter(_.isDirectory).flatMap(fileStream)).getOrElse({
      println("exception: dir cannot be listed: " + dir.getPath)
      LazyList.empty
    })

  def main(args: Array[String]) {
    // get the xml content from our sample file

    val textCenterRoot = args.head

    fileStream(new File(textCenterRoot))
      .filter(file => file.getName.endsWith("xml"))
      .map(filename => Tuple2(XML.loadFile(filename), filename))
      .map(tupleXmlFilename =>
        Tuple2(new RuleTransformer(TaskTypesRewriteRule()).transform(tupleXmlFilename._1), tupleXmlFilename._2))
      .foreach(xmlAndFilename => XML.save("output/" + xmlAndFilename._2.getName, xmlAndFilename._1.head))
  }

  /**
   *
   *
   * @param xml
   * @return all found correctanswers and wrongansers randomized and inside a <answer> element
   */
  def createAnswers(xml: Node) = {
    val correctAnswers: NodeSeq = xml \\ "correctanswer"
    val wrongAnswers: NodeSeq = xml \\ "wronganswer"

    val listOfAnswers: NodeSeq = correctAnswers ++ wrongAnswers
    val randomListOfAnswers = Random.shuffle(listOfAnswers)

    <answers>
      {copyWithoutScope(randomListOfAnswers)}
    </answers>
  }

  /**
   * Starts the correct RuleTransformers for each task type
   */
  case class TaskTypesRewriteRule() extends RewriteRule {
    override def transform(n: Node): Seq[Node] = n match {
      case e: Elem if List("singlechoice", "multiplechoice").contains(e.label) => addAnswersForChoiceTypes(n)
      case e: Elem if e.label == "draganddrop" => new RuleTransformer(FixDragAndDropTechnicalTaskText()).transform(e)
      case e: Elem if e.label == "clozepulldown" => new RuleTransformer(ClozePullDownRandomizer()).transform(e)
      case other => other
    }

    def addAnswersForChoiceTypes(xml: Node): Node = {
      val answers: Elem = createAnswers(xml)

      new RuleTransformer(
        RemoveElems("correctanswer", "correctanswers", "wronganswers"),
        AddAnswersTo("singlechoice", answers),
        AddAnswersTo("multiplechoice", answers)
      )
        .transform(xml)
        .head
    }
  }

  /**
   * sometimes the scala.xml adds a strange scope to child elems. This method mades sure that no scope is added.
   *
   * @param n
   * @return
   */
  def copyWithoutScope(n: Seq[Node]): Seq[Node] = {
    n collect {
      case e: Elem => e.copy(scope = TopScope, child = copyWithoutScope(e.child))
      case other => other
    }
  }

  /**
   *
   * @param label the elem label where to add the answers node
   * @param answers the node to add at the label
   */
  case class AddAnswersTo(label: String, answers: Node) extends RewriteRule {

    override def transform(n: Node): Node = n match {
      case elem: Elem if elem.label == label =>
        elem.copy(child = elem.child ++ answers)
      case n => n
    }
  }

  case class FixDragAndDropTechnicalTaskText() extends RewriteRule {
    override def transform(node: Node): Node = {
      node match {
        case e: Elem if e.label == "technicaltask" =>
          println(e.child)
          e.copy(child = e.child collect {
            case e: Text if e.text.contains("Zieh") => Text("Wähle die richtige(n) Antworten aus.")
            case e: Text if e.text.contains("Drag") => Text("Choose the correct answer(s).")
            case other => other
          })
        case other => other
      }
    }
  }

  /**
   *
   * @param labels to remove from xml
   */
  case class RemoveElems(labels: String*) extends RewriteRule {

    override def transform(n: Node): Seq[Node] = {
      n match {
        case e: Elem if labels.contains(e.label) => Seq.empty
        case other => other
      }
    }
  }

  /**
   * adds an <answers> element for each <segment> of CLozePullDown.
   * Removes old answer elements
   */
  case class ClozePullDownRandomizer() extends RewriteRule {
    override def transform(n: Node): Seq[Node] = {
      n match {
        case e: Elem if e.label == "segment" =>
          val answers = createAnswers(e)
          new RuleTransformer(
            RemoveElems("correctanswer", "wronganswers"),
            AddAnswersTo("segment", answers)).transform(e)
        case other => other
      }
    }
  }

}
